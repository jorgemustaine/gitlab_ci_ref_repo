# Deploy Runbot server

### Runbot dependencies

* matplotlib
* anybox.recipe.odoo

ambas instaladas vía pip en el contenedor accesado mediante:

~~~
$ docker exec -it --user=root your_container_id bash
~~~

* Runbot is a module part of addons-extra

###  Install Runbot server with docker

* [Runbot Docker](https://github.com/it-projects-llc/odoo-runbot-docker)
* [Runbot](https://www.odoo.yenthevg.com/installing-and-configuring-your-own-runbot/)

###   Github API v3

[Github API](https://developer.github.com/v3/)

### Ref Odoo Runbot:

* [odoo-development readthedocs](https://odoo-development.readthedocs.io/en/latest/ci/runbot.html)
* [slideshare runbot](chrome-extension://oemmndcbldboiebfnladdacbdfmadadm/https://www.odoo.com/slides/slide/how-to-setup-your-own-runbot-309/pdf_content)

### file sample runbot config

* [sample runbot config](https://github.com/gurneyalex/oca-runbot-setup)
* [sample vauxoo config](https://github.com/Vauxoo/runbot)
* [doodba](https://github.com/Tecnativa/doodba)

### Cabudare 08 de NOV de 2018
### Montevideo 04/04/2019

### Main stages of gitlab-ci:

* Building --> Code compilation in insolated env
* Testing --> run Unit Test, code analyzers, Performance Test
* Deploying --> pass the app to spec repo

### Install Gitlb-Runner over Gnu/Linux 

[Runner Install On Server](https://docs.gitlab.com/runner/install/linux-repository.html)


### Gitlab-ci architecture


![Arquitectura gitlab-ci](gitlab_ci_architecture.png)

## Gitlab CI/CD

* gitlab registry is similar to docker hub
* Se puede accesar al ci lint para checar nuestro .gitlag-ci.yml en: urlproject/-/ci/lint
* [.gitlab-ci.yml complete syntax](https://docs.gitlab.com/ee/ci/yaml/README.html)
* [Gitlab Runner Doc](https://docs.gitlab.com/ee/ci/runners/README.html)

### Resourses:

* [GitlabTuto](https://www.youtube.com/watch?v=34u4wbeEYEo&list=PLaFCDlD-mVOlnL0f9rl3jyOHNdHU--vlJ)
* [Pylint Doc and Tuto](https://pylint.readthedocs.io/en/latest/tutorial.html)
* [Ejemplo de integración](ttps://bloopark.de/blog/the-bloopark-times-english-2/post/odoo-erp-continuous-integration-and-project-management-263)
* [Vauxoo python pipelines](https://git.vauxoo.com/vauxoo/reqgen/blob/master/.gitlab-ci.yml)
* [pylint integration en gitlabci](https://stackoverflow.com/questions/43126475/pylint-badge-in-gitlab)
* [ejemplo de gitlab-ci.yml para odoo OCA](https://raw.githubusercontent.com/OCA/maintainer-quality-tools/master/sample_files/.gitlab-ci-2.yml)

**Ejemplos de gitlab-ci.yml for odoo: **

* [gitlabci.yml for odoo](https://gist.github.com/ovnicraft/c1fd582761f7e59720d4b7dff848f0eb)

### Sobre la gestión del software

* [Lean](https://es.wikipedia.org/wiki/Lean_software_development) premisas:
        
        
1. El desarrollo de software es un proceso de aprendizaje continuo.
1. La acumulación de defectos debe evitarse ejecutando las pruebas tan pronto como el código está escrito en lugar de añadir más documentación o planificación detallada.
1. El proceso de aprendizaje es acelerado con el uso iteraciones cortas cada una de ellas acompañada de refactorización y sus pruebas de integración.
1. Durante las reuniones, tanto los clientes como el equipo de desarrollo, logran aprender sobre el alcance del problema y buscan posibles soluciones para un mejor desarrollo.
1. Por lo tanto, los clientes comprenden mejor sus necesidades basándose en el resultado de los esfuerzos del desarrollo y los desarrolladores aprenden a satisfacer mejor estas necesidades.
1. Sprint y desarrollo ágil. (entregar tan pronto como sea posible reuniones de iteración menudas).
1. Kanban, el cliente dispone los requisitos necesarios. Esto podría ser simplemente presentar los requisitos en pequeñas fichas o historias y los desarrolladores estimarán el tiempo necesario para la aplicación de cada tarjeta.
1. cada mañana durante una reunión inicial cada miembro del equipo evalúa lo que se ha hecho ayer, lo que hay que hacer hoy y mañana y pregunta por cualquier nueva entrada necesaria de parte de sus colegas o del cliente.
1. Las repeticiones en el código son signo de un mal diseño de código y deben evitarse.
1. El completo y automatizado proceso de construcción debe ir acompañada de una suite completa y automatizada de pruebas, tanto para desarrolladores y clientes que tengan la misma versión, sincronización y semántica que el sistema actual.

* [Productividad del Software](https://samuelcasanova.com/2015/12/medir-la-productividad-en-un-equipo-de-software/)

1. Evaluaciones por equipo y no por personas. Promueve el trabajo colaborativo e impide el maquillaje de indicadores.

* [This is Lean / book resume](https://samuelcasanova.com/2019/03/resumen-this-is-lean/)

1. Las personas no estamos preparadas para muchos temas a la vez, lo que lleva al estrés y los errores (ej. muchos mails o proyectos a la vez).
1. la personas no estamos preparadas para muchos temas a la vez, lo que lleva al estrés y los errores (ej. muchos mails o proyectos a la vez).
1. La paradoja de la eficiencia consiste en creer que el trabajo hecho por satisfacer les necesidades secundarias de focalizarnos en eficiencia de recursos aportan valor, cuando son waste.
1. 7 tipos de waste: Sobreproducción, esperas, transporte, procesamiento de más, inventario, movimiento y defectos.

## LEAN (creado por toyota)

1. Trabajo en equipo.
1. Comunicación.
1. Uso eficiente de los recursos y eliminación del waste.
1. Mejoras continuas.

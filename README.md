[![License: CC BY 4.0](https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by/4.0/)

this repo is a referencial site about CI/CD with gitlab with didactical purpose

@jorgescalona 2019
### Cabudare 08 de NOV de 2018
### Montevideo 04/04/2019

### Main stages of gitlab-ci:

* Building --> Code compilation in insolated env
* Testing --> run Unit Test, code analyzers, Performance Test
* Deploying --> pass the app to spec repo

### Install Gitlb-Runner over Gnu/Linux 

[Runner Install On Server](https://docs.gitlab.com/runner/install/linux-repository.html)


### Gitlab-ci architecture


![Arquitectura gitlab-ci](static/images/gitlab_ci_architecture.png "Arquitectura Gitlab CI")


## Conventions and Guidelines

* [Guidelines OCA](https://github.com/OCA/odoo-community.org/blob/master/website/Contribution/CONTRIBUTING.rst)
* [maintainer quality tools OCA](https://github.com/OCA/maintainer-quality-tools)


**Branches names:**

ver-issue-author
8.0-id47-modify-strucrure-xml-jhondoe

**Commits:**


Here found the conventions adopt by community around odoo in world and good practices.


## Gitlab CI/CD

* gitlab registry is similar to docker hub
* Se puede accesar al ci lint para checar nuestro .gitlag-ci.yml en: urlproject/-/ci/lint
* [.gitlab-ci.yml complete syntax](https://docs.gitlab.com/ee/ci/yaml/README.html)
* [Gitlab Runner Doc](https://docs.gitlab.com/ee/ci/runners/README.html)

![CI vs CD](static/images/ci_vs_cd.png "CI vs CD")

### Resourses:

* [GitlabTuto](https://www.youtube.com/watch?v=34u4wbeEYEo&list=PLaFCDlD-mVOlnL0f9rl3jyOHNdHU--vlJ)
* [Pylint Doc and Tuto](https://pylint.readthedocs.io/en/latest/tutorial.html)
* [Ejemplo de integración](ttps://bloopark.de/blog/the-bloopark-times-english-2/post/odoo-erp-continuous-integration-and-project-management-263)
* [Vauxoo python pipelines](https://git.vauxoo.com/vauxoo/reqgen/blob/master/.gitlab-ci.yml)
* [pylint integration en gitlabci](https://stackoverflow.com/questions/43126475/pylint-badge-in-gitlab)
* [ejemplo de gitlab-ci.yml para odoo OCA](https://raw.githubusercontent.com/OCA/maintainer-quality-tools/master/sample_files/.gitlab-ci-2.yml)
* [Documento con referencias varias](https://docs.google.com/document/d/1HShefEUDmBvQKNAMQI_OgCnq1H83LQ8wcPRcrpafPes/edit?usp=sharing)
* [Como hacer CD desde casa](https://medium.com/free-code-camp/how-to-set-up-continuous-deployment-in-your-home-project-the-easy-way-41b84a467eed)



**Ejemplos de gitlab-ci.yml for odoo: **



* [gitlabci.yml for odoo](https://gist.github.com/ovnicraft/c1fd582761f7e59720d4b7dff848f0eb)


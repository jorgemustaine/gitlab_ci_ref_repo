# Roadmap

Lograr convenciones internas con referentes teóricos reales adaptados al
entorno. Estás deben incluir consideraciones respecto de:

* Git
    1. gobernanza
        * repositorios
        * branches


* Equipos de pruebas
    1. Functionals
    1. Devs
    1. Security Revisors, security specialist
        * Equipos de sguridad, calidad y desarrollo
    1. Ifrastructure Team

* Runiones periodicas o menudas.
    1. Like Scrum or agile.

#  Git For Teams

***"En equipos pequeños, puede tener una persona que desempeña muchos roles. Es
relativamente fácil mantenerse en contacto con todas las actividades diarias de
todos en un equipo pequeño. Sin embargo, en equipos grandes, puede tener roles
separados en diferentes departamentos."***

***"alguien a quien se le pide que haga demasiado sin la cantidad adecuada de
contexto definitivamente perderá algo"***

El usuario. ***"en el desarrollo basado en pruebas, redactará la prueba de
aceptación para que tenga una definición de cómo sabrá que se ha resuelto el
problema."***

***"Por lo general, la prueba debe ser descriptiva del problema a resolver, no
prescriptiva de la tecnología que se debe utilizar."***

***"Idealmente, tendrá scripts de compilación que se pueden usar para duplicar
automáticamente tanto como sea posible. Incluso puede optar por trabajar con
Docker y / o Vagrant para crear una réplica exacta de su entorno."***

***"Cuanto más tiempo tenga que esperar el código para una revisión, más probable
es que se haya desviado de la rama principal del trabajo."***

esto ultimo versa con la agilidad que se **suministre**, se **testee** o se **despliegue**
el código.

***"El truco para un equipo motivado y cohesivo es respetar a cada una de las
personas del equipo y, cuando sea posible, optimizar el proceso para adaptarlo
a sus preferencias."***

#  Contenerización

**Kubernets:** plataforma diseñada para administrar completamente el ciclo de
vida de las aplicaciones y servicios en contenedores utilizando métodos que
proporcionan previsibilidad, escalabilidad y alta disponibilidad.

**K** es un  orquestador de contenedores en un cluster donde cada uno de ellos se
denomina **nodos**. Estos últimos pueden ser (docker, rtk, etc).

**K** es una solución ideal para sistemas distribuidos donde se requiere orquestación
de microservicios. Odoo al ser una aplicación monolítica no se beneficia de la
administración de nodos que hace el Server Master de **K**.



[Curso Intro Kubernets](https://www.digitalocean.com/community/tutorials/an-introduction-to-kubernetes "Curso Kubernet")

![Kubernet Arch](static/images/kube_arch.png)

[Enlace Libros Interes](https://mega.nz/#F!HuQ2nCBb "Libros interes mega")

# Gobernanza de repositorios

Según el libro [GitForTeams](http://gitforteams.com/ "gft")

1. Modelos de liderazgo:
    * Dictador benevolo:
    * Avance concensuado, aprobación del lider:
    * Revisión técnica o Comite de gestión:

1. Que gobernar:
    * Repositorios: Aquí se define el modelo a seguir para contribuir al
    proyecto.
    * Branches: Define la forma como bifurcamos el trabajo para aislar los cambios,
    Estratégias de creación de ramas (v1,v2..... master, testing....), aquí se
    debe definir el nombramiento de las ramas, los mensajes de commit,
    y la estructura de solicitud de mezclas al código padre.
    * Merge Requests: Definir los parámetros mínimos de forma y calidad para que
    la solicitud pueda ser aceptada.

# Branches

Cada rama es una clección de confirmaciones (**commits**), que estan vinculadas entre
sí a través de sus metadatos y una referencia única [sha](https://es.wikipedia.org/wiki/Secure_Hash_Algorithm).

![Hospedaje de repositorio centralizado](static/images/centralized_code_hosting.png "Hospedaje centralizado")

#  Convenciones

* [PEP-8](https://www.python.org/dev/peps/pep-0008/ "Python pep")
* [OCA Guidelines](https://github.com/OCA/odoo-community.org/blob/master/website/Contribution/CONTRIBUTING.rst "Guidelines of OCA")
    * **Lints:** 
        * [Pylint](https://www.pylint.org "pylint")
        * [Flake8](http://flake8.pycqa.org/en/latest/ "pylint")

### Configure Pycharm with external Lints

![File/Settings](static/images/pycharm_conf_external_lint_00.png "Pycharm confg")
![External Tools](static/images/pycharm_conf_external_lint_01.png "Pycharm ext lint")

**parámetros de configuración:**

* flake8

    * Program:  $PyInterpreterDirectory$/python
    * Arguments: -m flake8 --max-complexity 10 $FilePath$
    * Working directory: $ProjectFileDir$

* Pylint

    * Program: $PyInterpreterDirectory$/python
    * Arguments: -m pylint $FilePath$
    * Working directory: $ProjectFileDir$

